@extends('template')

@section('content')
    <div class="col-lg-12">
    <h1>
        Trace Links
    </h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Origin Type</td>
                <td>Target Type</td>
                <td>Origin Description</td>
                <td>Target Description</td>
            </tr>
        </thead>
        <tbody>
            @foreach($traceLinks as $tl)
                <tr>
                    <td>{{ $tl->id }}</td>
                    <td>{{ $tl->origin_type }}</td>
                    <td>{{ $tl->target_type }}</td>
                    <td>{{ $tl->origin->description }}</td>
                    <td>{{ $tl->target->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection