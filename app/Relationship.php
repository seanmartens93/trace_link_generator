<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    //
    public function domain()
    {
        return $this->belongsTo('App\OWL_Class', 'domain_class_id');
    }

    //
    public function range()
    {
        return $this->belongsTo('App\OWL_Class', 'range_class_id');
    }
}
