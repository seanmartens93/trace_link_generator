<?php

use Illuminate\Database\Seeder;

class ClassTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('class_types')->insert([
            'name' => "Model"
        ]);

        //2
        DB::table('class_types')->insert([
            'name' => "View"
        ]);

        //3
        DB::table('class_types')->insert([
            'name' => "Controller"
        ]);

        //4
        DB::table('class_types')->insert([
            'name' => "Feature Test"
        ]);

        //5
        DB::table('class_types')->insert([
            'name' => "UI Test"
        ]);
    }
}
