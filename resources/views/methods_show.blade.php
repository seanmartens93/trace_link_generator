@extends('template')

@section('content')

    <h1>Method #{{ $method->id }}: {{ $method->name }}</h2>
    <br>
    Belongs to class: {{ $method->codeClass->name }}

    <h1>Domain Concepts</h2>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($method->codeClass->domainConcepts as $concept)
                <tr>
                    <td>{{ $concept->id }}</td>
                    <td>{{ $concept->concept_name }}</td>
                </tr>
            @endforeach
            @foreach($method->domainConcepts as $concept)
                <tr>
                    <td>{{ $concept->id }}</td>
                    <td>{{ $concept->concept_name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection