<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    public function codeClass()
    {
        return $this->belongsTo('App\CodeClass', 'code_class_id');
    }

    public function domainConcepts()
    {
        return $this->hasMany('App\DomainConcept');
    }
}
