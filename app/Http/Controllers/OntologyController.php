<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ontology;
use App\OWL_Class;
use App\Relationship;
use App\UserStory;
use App\CodeClass;
use App\Method;
use App\TraceLink;

class OntologyController extends Controller
{
    //
    public function overview()
    {
        $ontology = Ontology::all();

        $classes = OWL_Class::all();

        $relationships = Relationship::all();

        return view('ontology_overview')->withOntology($ontology)->withClasses($classes)->withRelationships($relationships);
    }

    public function visualization()
    {
        $ontology = Ontology::all();

        $classes = OWL_Class::all();

        $relationships = Relationship::all();

        return view('ontology_visualization')->withOntology($ontology)->withClasses($classes)->withRelationships($relationships);
    }

    public function artifacts()
    {
        $user_stories = UserStory::all();

        $classes = CodeClass::all();

        $methods = Method::all();

        return view('artifacts')->withUserStories($user_stories)->withClasses($classes)->withMethods($methods);
    }
    
    public function tracelinks()
    {
        $trace_links = TraceLink::all();

        return view('tracelinks')->withTraceLinks($trace_links);
    }
}
