<?php

use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('owl_classes')->insert([
            'name' => "Administrator",
            'ontology_id' => 1
        ]);

        // 2
        DB::table('owl_classes')->insert([
            'name' => "Event",
            'ontology_id' => 1
        ]);
        
        // 3
        DB::table('owl_classes')->insert([
            'name' => "Member",
            'ontology_id' => 1
        ]);
        
        // 4
        DB::table('owl_classes')->insert([
            'name' => "Group",
            'ontology_id' => 1
        ]);
        
        // 5
        DB::table('owl_classes')->insert([
            'name' => "Email",
            'ontology_id' => 1
        ]);
        
        // 6
        DB::table('owl_classes')->insert([
            'name' => "Datum",
            'ontology_id' => 1
        ]);
        
        // 7
        DB::table('owl_classes')->insert([
            'name' => "Cost",
            'ontology_id' => 1
        ]);

        // 8
        DB::table('owl_classes')->insert([
            'name' => "Due",
            'ontology_id' => 1
        ]);
        
        // 9
        DB::table('owl_classes')->insert([
            'name' => "System",
            'ontology_id' => 1
        ]);
        
        // 10
        DB::table('owl_classes')->insert([
            'name' => "Report",
            'ontology_id' => 1
        ]);

        // 11
        DB::table('owl_classes')->insert([
            'name' => "Price",
            'ontology_id' => 1
        ]);
        
        // 12
        DB::table('owl_classes')->insert([
            'name' => "Type",
            'ontology_id' => 1
        ]);
        
        // 13
        DB::table('owl_classes')->insert([
            'name' => "Overview",
            'ontology_id' => 1
        ]);
        
        // 14
        DB::table('owl_classes')->insert([
            'name' => "Date",
            'ontology_id' => 1
        ]);
        
        // 15
        DB::table('owl_classes')->insert([
            'name' => "DateRange",
            'ontology_id' => 1
        ]);
        
        // 16
        DB::table('owl_classes')->insert([
            'name' => "Confirmation",
            'ontology_id' => 1
        ]);
        
        //17
        DB::table('owl_classes')->insert([
            'name' => "Text",
            'ontology_id' => 1
        ]);

        //18
        DB::table('owl_classes')->insert([
            'name' => "Information",
            'ontology_id' => 1
        ]);

        //19
        DB::table('owl_classes')->insert([
            'name' => "Time",
            'ontology_id' => 1
        ]);

        //20
        DB::table('owl_classes')->insert([
            'name' => "Trainer",
            'ontology_id' => 1
        ]);
    }
}
