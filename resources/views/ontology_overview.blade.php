@extends('template')

@section('content')

    <h1>
        Ontology Overview
    </h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($classes as $class)
                <tr>
                    <td>{{ $class->id }}</td>
                    <td>{{ $class->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
            <td>Domain</td>
			<td>Name</td>
            <td>Range</td>
		</tr>
	</thead>
        <tbody>
            @foreach($relationships as $relationship)
                <tr>
                    <td>{{ $relationship->id }}</td>
                    <td>{{ $relationship->domain->name }}</td>
                    <td>{{ $relationship->name }}</td>
                    <td>{{ $relationship->range->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


@endsection