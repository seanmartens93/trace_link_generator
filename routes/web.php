<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/generator', function () {
    return view('generator');
})->name('generator');

Route::get('/ontology', 'OntologyController@overview')->name('ontology');

Route::get('/visualization', 'OntologyController@visualization')->name('visualization');

Route::get('/artifacts', 'OntologyController@artifacts')->name('artifacts');

Route::get('/tracelinks', 'OntologyController@tracelinks')->name('tracelinks');

Route::resource('methods','MethodController');

Route::resource('classes', 'CodeClassesController');