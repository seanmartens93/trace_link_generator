<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_concepts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concept_name');
            
            $table->integer('code_class_id')->unsigned()->index()->nullable();
            $table->foreign('code_class_id')->references('id')->on('code_classes');

            $table->integer('method_id')->unsigned()->index()->nullable();
            $table->foreign('method_id')->references('id')->on('methods');

            $table->integer('user_story_id')->unsigned()->index()->nullable();
            $table->foreign('user_story_id')->references('id')->on('user_stories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_concepts');
    }
}
