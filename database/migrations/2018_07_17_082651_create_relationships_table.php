<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            
            $table->integer('domain_class_id')->unsigned()->index()->nullable();
            $table->foreign('domain_class_id')->references('id')->on('owl_classes');

            $table->integer('range_class_id')->unsigned()->index()->nullable();
            $table->foreign('range_class_id')->references('id')->on('owl_classes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationships');
    }
}
