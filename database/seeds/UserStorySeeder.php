<?php

use Illuminate\Database\Seeder;

class UserStorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_stories')->insert([
            'description' => "As a member, I want to register in the system, so that my membership is registered with the association.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a member, I want to receive a confirmation email, so that I know my registration was successful.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a member, I want to receive emails from my trainer, so Im informed of upcoming events.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to create events, so that I can manage events in the system.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to read events, so that I can see information of a specific event.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to update events, so that I can change its information.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to delete events, so that I can cancel events.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to see an overview of events in a calendar, so that I can see upcoming events.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to add members to an events, so that I can let them participate.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to select if an event is billable, so that I can let it count for the dues.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to set extra costs to an event, so that I can bill extra costs.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to create a group, so that I can manage groups.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to add users to a group, so that I can manage who is in the group.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to read a group, so that I can see who is in the group.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to update a group, so that I can update who is in the group.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to delete a group, so that I can remove redundant groups.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to set a type to an event, so that I can indicate what type of event it is.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to change data of members, so that I can update their data in case of a change.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to generate a report, so that I can see the dues to be paid by a member.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to generate a report for trainers, so that I can see the hours they worked.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a member, I want to know the dues to be paid, so that I can pay them.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a member, I want to receive an email when new dues have to be paid, so that I can pay them.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a trainer, I want to see events, so that I know which events are upcoming.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a trainer, I want to email groups of members, so that I can share information with my groups.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As a trainer, I want to email a member, so that I can share information with them.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to assign costs to the first hour of events, so that I can manage the prices.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to assign costs to extra hours of events, so that I can manage the prices.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to know the costs per member, so that I can bill the dues.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to set the welcome text after registration, so that I can update the confirmation email.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to send an email with the dues, so that my members know the amount of dues to be paid.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to see a home screen, so that I get a confirmation that I logged in.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to set a date range on the calendar, so that I can see events over time.",
        ]);

        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to select a group in the calendar, so that I can only see events attached to that group.",
        ]);

        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to create a member, so that I can add the member when they didnt register.",
        ]);
        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to select if a member is a digimember, so that I can manage which members are digimembers.",
        ]);
        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to search members, so that I can find a certain member.",
        ]);
        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to search groups, so that I can find a certain group.",
        ]);
        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to search events, so that I can find a certain event.",
        ]);
        
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to set a date range on a report, so that I can generate a report on a period of time.",
        ]);
                
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to login, so that I can access the system.",
        ]);
                
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to logout, so that I can safely leave the system.",
        ]);
                
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to receive a password reset link, so that I can reset my password if I forget it.",
        ]);
                
        DB::table('user_stories')->insert([
            'description' => "As an administrator, I want to remember my login data, so that I can stay logged in for a longer time.",
        ]);
            
        DB::table('user_stories')->insert([
            'description' => "As a user, I want to see my name in the system, so that I know Im logged in with the correct account.",
        ]);
    }
}
