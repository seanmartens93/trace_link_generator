@extends('template')

@section('content')

    <br>
    <h1>
        Ontology Visualization
    </h1>
    <div id="mynetwork"></div>

    <br>
    <h1>
        Ontology Overview
    </h1>

    <br>
    <h2>Concepts</h2>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($classes as $class)
                <tr>
                    <td>{{ $class->id }}</td>
                    <td>{{ $class->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h2>Relations</h2>

    <table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
            <td>Domain</td>
			<td>Name</td>
            <td>Range</td>
		</tr>
	</thead>
        <tbody>
            @foreach($relationships as $relationship)
                <tr>
                    <td>{{ $relationship->id }}</td>
                    <td>{{ $relationship->domain->name }}</td>
                    <td>{{ $relationship->name }}</td>
                    <td>{{ $relationship->range->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script type="text/javascript">
        // create an array with nodes
        var nodes = new vis.DataSet([
            @foreach ($classes as $class)
                {id: parseInt("{{$class->id}}"), label: "{{$class->name}}", color: "#007bff", font: {size: 18, color: 'white', face: 'arial'}},
            @endforeach
        ]);

        // create an array with edges
        var edges = new vis.DataSet([
            @foreach ($relationships as $relationship)
                {from: parseInt("{{$relationship->domain_class_id}}"), to: "{{$relationship->range_class_id}}", label: "{{$relationship->name}}"},
            @endforeach
        ]);

        // create a network
        var container = document.getElementById('mynetwork');

        // provide the data in the vis format
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            shape: 'dot',
            size: 10
        };

        // initialize your network!
        var network = new vis.Network(container, data, options);
    </script>

@endsection