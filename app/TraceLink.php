<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraceLink extends Model
{
    //

    public function origin()
    {
        return $this->belongsTo('App\UserStory', 'origin_user_story_id');
    }
    
    public function target()
    {
        return $this->belongsTo('App\CodeClass', 'target_code_class_id');
    }
}
