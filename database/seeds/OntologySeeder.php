<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OntologySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ontologies')->insert([
            'name' => "ontology"
        ]);
    }
}
