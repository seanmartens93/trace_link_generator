<?php

use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Administrator Create Event
        DB::table('relationships')->insert([
            'name' => "Create",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Manage Event
        DB::table('relationships')->insert([
            'name' => "Manage",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Delete Event
        DB::table('relationships')->insert([
            'name' => "Delete",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Search Event
        DB::table('relationships')->insert([
            'name' => "Search",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);
        
        // Administrator Update Event
        DB::table('relationships')->insert([
            'name' => "Update",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);
        
        // Administrator See Event
        DB::table('relationships')->insert([
            'name' => "See",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Indicate Event
        DB::table('relationships')->insert([
            'name' => "Indicate",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Cancel Event
        DB::table('relationships')->insert([
            'name' => "Cancel",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);
        
        // Administrator Find Event
        DB::table('relationships')->insert([
            'name' => "Find",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);
        
        // Administrator Read Event
        DB::table('relationships')->insert([
            'name' => "Read",
            'domain_class_id' => 1,
            'range_class_id' => 2
        ]);

        // Administrator Find Member
        DB::table('relationships')->insert([
            'name' => "Find",
            'domain_class_id' => 1,
            'range_class_id' => 3
        ]);

        // Administrator Create Member
        DB::table('relationships')->insert([
            'name' => "Create",
            'domain_class_id' => 1,
            'range_class_id' => 3
        ]);

        // Administrator Add Member
        DB::table('relationships')->insert([
            'name' => "Add",
            'domain_class_id' => 1,
            'range_class_id' => 3
        ]);

        // Administrator Search Member
        DB::table('relationships')->insert([
            'name' => "Search",
            'domain_class_id' => 1,
            'range_class_id' => 3
        ]);

        // Administrator Send Email
        DB::table('relationships')->insert([
            'name' => "Send",
            'domain_class_id' => 1,
            'range_class_id' => 5
        ]);

        // Administrator Change Datum
        DB::table('relationships')->insert([
            'name' => "Change",
            'domain_class_id' => 1,
            'range_class_id' => 6
        ]);

        // Administrator Update Datum
        DB::table('relationships')->insert([
            'name' => "Update",
            'domain_class_id' => 1,
            'range_class_id' => 6
        ]);

        // Administrator Bill Cost
        DB::table('relationships')->insert([
            'name' => "Bill",
            'domain_class_id' => 1,
            'range_class_id' => 7
        ]);

        // Administrator Set Cost
        DB::table('relationships')->insert([
            'name' => "Set",
            'domain_class_id' => 1,
            'range_class_id' => 7
        ]);

        // Administrator Know Cost
        DB::table('relationships')->insert([
            'name' => "Know",
            'domain_class_id' => 1,
            'range_class_id' => 7
        ]);

        // Administrator Assign Cost
        DB::table('relationships')->insert([
            'name' => "Assign",
            'domain_class_id' => 1,
            'range_class_id' => 7
        ]);

        // Administrator Let Due
        DB::table('relationships')->insert([
            'name' => "Let",
            'domain_class_id' => 1,
            'range_class_id' => 8
        ]);

        // Administrator Bill Due
        DB::table('relationships')->insert([
            'name' => "Bill",
            'domain_class_id' => 1,
            'range_class_id' => 8
        ]);

        // Administrator See Due
        DB::table('relationships')->insert([
            'name' => "See",
            'domain_class_id' => 1,
            'range_class_id' => 8
        ]);
        
        // Administrator Select System
        DB::table('relationships')->insert([
            'name' => "Select",
            'domain_class_id' => 1,
            'range_class_id' => 9
        ]);

        // Administrator Access System
        DB::table('relationships')->insert([
            'name' => "Access",
            'domain_class_id' => 1,
            'range_class_id' => 9
        ]);

        // Administrator Leave System
        DB::table('relationships')->insert([
            'name' => "Leave",
            'domain_class_id' => 1,
            'range_class_id' => 9
        ]);

        // Administrator Login System
        DB::table('relationships')->insert([
            'name' => "Login",
            'domain_class_id' => 1,
            'range_class_id' => 9
        ]);

        // Administrator Logout System
        DB::table('relationships')->insert([
            'name' => "Logout",
            'domain_class_id' => 1,
            'range_class_id' => 9
        ]);

        // Administrator Generate Report
        DB::table('relationships')->insert([
            'name' => "Generate",
            'domain_class_id' => 1,
            'range_class_id' => 10
        ]);

        // Administrator Find Group
        DB::table('relationships')->insert([
            'name' => "Find",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Select Group
        DB::table('relationships')->insert([
            'name' => "Select",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Delete Group
        DB::table('relationships')->insert([
            'name' => "Delete",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Create Group
        DB::table('relationships')->insert([
            'name' => "Create",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Remove Group
        DB::table('relationships')->insert([
            'name' => "Remove",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Search Group
        DB::table('relationships')->insert([
            'name' => "Search",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Read Group
        DB::table('relationships')->insert([
            'name' => "Read",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Manage Group
        DB::table('relationships')->insert([
            'name' => "Manage",
            'domain_class_id' => 1,
            'range_class_id' => 4
        ]);

        // Administrator Change Information
        DB::table('relationships')->insert([
            'name' => "Change",
            'domain_class_id' => 1,
            'range_class_id' => 18
        ]);

        // Administrator See Information
        DB::table('relationships')->insert([
            'name' => "See",
            'domain_class_id' => 1,
            'range_class_id' => 18
        ]);

        // Administrator Manage Price
        DB::table('relationships')->insert([
            'name' => "Manage",
            'domain_class_id' => 1,
            'range_class_id' => 11
        ]);

        // Administrator Set Text
        DB::table('relationships')->insert([
            'name' => "Set",
            'domain_class_id' => 1,
            'range_class_id' => 17
        ]);

        // Administrator Update Confirmation
        DB::table('relationships')->insert([
            'name' => "Update",
            'domain_class_id' => 1,
            'range_class_id' => 16
        ]);

        // Administrator Get Confirmation
        DB::table('relationships')->insert([
            'name' => "Get",
            'domain_class_id' => 1,
            'range_class_id' => 16
        ]);

        // Adminstartor Set Date
        DB::table('relationships')->insert([
            'name' => "Set",
            'domain_class_id' => 1,
            'range_class_id' => 14
        ]);

        // Administrator Set DateRange
        DB::table('relationships')->insert([
            'name' => "Set",
            'domain_class_id' => 1,
            'range_class_id' => 15
        ]);

        // Administrator See Overview
        DB::table('relationships')->insert([
            'name' => "See",
            'domain_class_id' => 1,
            'range_class_id' => 13
        ]);

        // Administrator Set Type
        DB::table('relationships')->insert([
            'name' => "Set",
            'domain_class_id' => 1,
            'range_class_id' => 12
        ]);

        // Administrator Stay Time
        DB::table('relationships')->insert([
            'name' => "Stay",
            'domain_class_id' => 1,
            'range_class_id' => 19
        ]);

        // Member Receive Email
        DB::table('relationships')->insert([
            'name' => "Receive",
            'domain_class_id' => 3,
            'range_class_id' => 5
        ]);

        // Member Know Due
        DB::table('relationships')->insert([
            'name' => "Know",
            'domain_class_id' => 3,
            'range_class_id' => 8
        ]);

        // Member Inform Of Email
        DB::table('relationships')->insert([
            'name' => "Inform Of",
            'domain_class_id' => 3,
            'range_class_id' => 5
        ]);

        // Member Register In System
        DB::table('relationships')->insert([
            'name' => "Register In",
            'domain_class_id' => 3,
            'range_class_id' => 9
        ]);

        // Trainer Email Group
        DB::table('relationships')->insert([
            'name' => "Email",
            'domain_class_id' => 20,
            'range_class_id' => 4
        ]);

        // Trainer See Event
        DB::table('relationships')->insert([
            'name' => "See",
            'domain_class_id' => 20,
            'range_class_id' => 2
        ]);
        
        // Trainer Email Member
        DB::table('relationships')->insert([
            'name' => "Email",
            'domain_class_id' => 20,
            'range_class_id' => 3
        ]);
    }
}
