@extends('template')

@section('content')

    <div class="title m-b-md" style="padding-top: 20%">
        Press Button to Generate Trace Links!
    </div>

    <div class="links">
        <button type="button" class="btn btn-primary btn-lg"><strong>Generate</strong></button>
    </div>

@endsection