<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OntologySeeder::class);
        $this->call(ClassSeeder::class);
        $this->call(RelationshipSeeder::class);
        $this->call(UserStorySeeder::class);
        $this->call(ClassTypeSeeder::class);
    }
}
