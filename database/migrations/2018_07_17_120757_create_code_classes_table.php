<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            
            $table->integer('class_type_id')->unsigned()->index()->nullable();
            $table->foreign('class_type_id')->references('id')->on('class_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_classes');
    }
}
