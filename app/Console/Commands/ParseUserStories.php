<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\UserStory;
use App\DomainConcept;

use Illuminate\Support\Facades\DB;

class ParseUserStories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParseUserStories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ParseUserStories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user_stories = UserStory::all();

        foreach($user_stories as $us)
        {
            $domain_concepts = [];

            $clean_story = preg_replace('/[^\p{L}\p{N}\s]/u', '', $us->description);

            $terms = explode(" ", $clean_story);

            foreach($terms as $t)
            {
                if ($t != "As" && $t != "a" && $t != "an" && $t != "I" && $t != "want" && $t != "to" && $t != "so" && $t != "that" && $t != "the" && $t != "my" && $t != "is")
                {
                    array_push($domain_concepts, $t);
                }
            }

            foreach($domain_concepts as $dc)
            {
                DB::table('domain_concepts')->insertGetId([
                    'concept_name' => $dc,
                    'user_story_id' => $us->id,
                ]);
            }
        }
    }
}
