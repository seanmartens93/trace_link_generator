<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserStory;
use App\CodeClass;
use App\Method;
use App\Ontology;
use App\OWL_Class;
use App\Relationship;
use App\ClassType;
use App\DomainConcept;
use App\TraceLink;
use Illuminate\Support\Facades\DB;

class GenerateTraceLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generatetracelinks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates Trace Links';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Tracelink::truncate();
        
        $user_stories = UserStory::all();
        $models = CodeClass::where('class_type_id', '=', 1)->get();
        $controllers = CodeClass::where('class_type_id', '=', 3)->get();
        $feature_tests = CodeClass::where('class_type_id', '=', 4)->get();

        //$test_us = UserStory::find(4);
        //$test_ft = CodeClass::find(8);

        //$us = $user_stories->first();
        //$m = $models->first();

        foreach ($user_stories as $us)
        {
            // if ($us->id == 32)
            // {
                foreach ($feature_tests as $ft)
                {
                    $sub_ontology_us = $this->generateSubOntology($us->domainConcepts, false);
                    $sub_ontology_ft = $this->generateSubOntology($ft->domainConcepts, true);
                    
                    if(count($sub_ontology_us->class_ids) > 1)
                    {
                        $similarity = $this->mapOntologies($sub_ontology_us, $sub_ontology_ft);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'User Story',
                                'target_type' => 'Feature Test',
                                'origin_user_story_id' => $us->id,
                                'target_code_class_id' => $ft->id,
                            ]);
                        }
                    }
                }
            // }
        }

        foreach ($user_stories as $us)
        {
            // if ($us->id == 32)
            // {
                foreach ($models as $m)
                {
                    $sub_ontology_us = $this->generateSubOntology($us->domainConcepts, false);
                    $sub_ontology_m = $this->generateSubOntology($m->domainConcepts, true);
                    
                    if(count($sub_ontology_us->class_ids) > 1)
                    {
                        $similarity = $this->mapOntologies($sub_ontology_us, $sub_ontology_m);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'User Story',
                                'target_type' => 'Model',
                                'origin_user_story_id' => $us->id,
                                'target_code_class_id' => $m->id,
                            ]);
                        }
                    }
                }
            // }
        }

        foreach ($user_stories as $us)
        {
            // if ($us->id == 32)
            // {
                foreach ($controllers as $c)
                {
                    $sub_ontology_us = $this->generateSubOntology($us->domainConcepts, false);
                    $sub_ontology_c = $this->generateSubOntology($c->domainConcepts, true);
                    
                    if(count($sub_ontology_us->class_ids) > 1)
                    {
                        $similarity = $this->mapOntologies($sub_ontology_us, $sub_ontology_c);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'User Story',
                                'target_type' => 'Controller',
                                'origin_user_story_id' => $us->id,
                                'target_code_class_id' => $c->id,
                            ]);
                        }
                    }
                }
            // }
        }

        foreach ($models as $m)
        {
            // if ($us->id == 32)
            // {
                foreach ($controllers as $c)
                {
                    $sub_ontology_m = $this->generateSubOntology($m->domainConcepts, false);
                    $sub_ontology_c = $this->generateSubOntology($c->domainConcepts, true);
                    
                    // if(count($sub_ontology_us->class_ids) > 1)
                    // {
                        $similarity = $this->mapOntologies($sub_ontology_m, $sub_ontology_c);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'Model',
                                'target_type' => 'Controller',
                                'origin_user_story_id' => $m->id,
                                'target_code_class_id' => $c->id,
                            ]);
                        }
                    // }
                }
            // }
        }

        foreach ($models as $m)
        {
            // if ($us->id == 32)
            // {
                foreach ($feature_tests as $ft)
                {
                    $sub_ontology_m = $this->generateSubOntology($m->domainConcepts, false);
                    $sub_ontology_ft = $this->generateSubOntology($ft->domainConcepts, true);
                    
                    // if(count($sub_ontology_us->class_ids) > 1)
                    // {
                        $similarity = $this->mapOntologies($sub_ontology_m, $sub_ontology_ft);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'Model',
                                'target_type' => 'Feature Test',
                                'origin_user_story_id' => $m->id,
                                'target_code_class_id' => $c->id,
                            ]);
                        }
                    // }
                }
            // }
        }

        foreach ($controllers as $c)
        {
            // if ($us->id == 32)
            // {
                foreach ($feature_tests as $ft)
                {
                    $sub_ontology_c = $this->generateSubOntology($c->domainConcepts, false);
                    $sub_ontology_ft = $this->generateSubOntology($ft->domainConcepts, true);
                    
                    // if(count($sub_ontology_us->class_ids) > 1)
                    // {
                        $similarity = $this->mapOntologies($sub_ontology_c, $sub_ontology_ft);

                        if ($similarity > 0.9)
                        {
                            DB::table('trace_links')->insertGetId([
                                'origin_type' => 'Controller',
                                'target_type' => 'Feature Test',
                                'origin_user_story_id' => $m->id,
                                'target_code_class_id' => $c->id,
                            ]);
                        }
                    // }
                }
            // }
        }

        $done = 'done';
    }

    public function generateSubOntology($domain_concepts, $without_roles)
    {
        $ontology = Ontology::all()->first();

        $classes = $ontology->owl_classes;

        // TODO: belongs to ontology
        $relationships = Relationship::all();

        $class_ids = [];
        $relationship_ids = [];
        $relationship_class_ids = [];

        foreach ($classes as $c)
        {
            $owl_class_name = strtolower($c->name);
            foreach ($domain_concepts as $dc)
            {
                $domain_concept_name = strtolower($dc->concept_name);
                
                $similarity = similar_text($domain_concept_name, $owl_class_name, $perc);

                if ($perc > 75)
                {
                    array_push($class_ids, $c->id);
                }
            }
        }

        foreach ($relationships as $r)
        {
            $arname = $r->name;

            foreach ($domain_concepts as $dc)
            {
                $domain_concept_name = strtolower($dc->concept_name);
                $relationship_name = strtolower($r->name);
                $domain_name = strtolower($r->domain->name);
                $range_name = strtolower($r->range->name);

                $similarity1 = similar_text($domain_concept_name, $relationship_name, $perc1);
                $similarity2 = similar_text($domain_concept_name, $domain_name, $perc2);
                $similarity3 = similar_text($domain_concept_name, $range_name, $perc3);

                if ($perc1 > 90)
                {
                    foreach($domain_concepts as $dc2)
                    {
                        // check domain an range of relation, if 1 of them is in the DC's, add them all to sub onto
                        $similarity2 = similar_text(strtolower($dc2->concept_name), $range_name, $perc2);

                        if ($perc2 > 75)
                        {
                            foreach($domain_concepts as $dc3)
                            {
                                // check domain an range of relation, if 1 of them is in the DC's, add them all to sub onto
                                $similarity3 = similar_text(strtolower($dc3->concept_name), $domain_name, $perc3);
                                
                                if ($perc3 > 75 || $without_roles)
                                {
                                    array_push($relationship_ids, $r->id);
                                    array_push($relationship_class_ids, $r->domain->id);
                                    array_push($relationship_class_ids, $r->range->id);
                                }
                            }
                        }
                    }
                }
            }
        }

        $sub_ontology = (object)[
            'class_ids' => array_unique($class_ids),
            'relationship_ids' => array_unique($relationship_ids),
            'relationship_class_ids' => array_unique($relationship_class_ids),
        ];

        return $sub_ontology;
    }

    public function mapOntologies($ontology1, $ontology2)
    {
        //TODO: Map 2 ontologies and return similarity score
        $ont1_classes = $ontology1->class_ids;
        $ont2_classes = $ontology2->class_ids;

        $ont1_relclasses = $ontology1->relationship_class_ids;
        $ont2_relclasses = $ontology2->relationship_class_ids;

        $classes1 = array_unique(array_merge($ont1_classes, $ont1_relclasses));
        $classes2 = array_unique(array_merge($ont2_classes, $ont2_relclasses));

        $similar_classes = 0;

        //Foreach class, check if class is in other ontology
        foreach ($classes1 as $ont1_class)
        {
            foreach ($classes2 as $ont2_class)
            {
                if ($ont1_class == $ont2_class)
                {
                    $similar_classes++;
                }
            }
        }

        $ont1_relationships = $ontology1->relationship_ids;
        $ont2_relationships = $ontology2->relationship_ids;

        $similar_relationships = 0;

        //Foreach relationhsip, check if relation is in other ontology
        foreach ($ont1_relationships as $ont1_rel)
        {
            foreach ($ont2_relationships as $ont2_rel)
            {
                if ($ont1_rel == $ont2_rel)
                {
                    $similar_relationships++;
                }
            }
        }

        $similar_elements = $similar_classes + $similar_relationships;

        $amount_of_elements_ont1 = count($classes1) + count($ont1_relationships);
        $amount_of_elements_ont2 = count($classes2) + count($ont2_relationships);
        //Matches/amount_of_elements * 100 = score

        if ($amount_of_elements_ont1 > 0 && $amount_of_elements_ont2 > 0)
        {
            //A-B Trace
            $sim_a_b = $similar_elements / $amount_of_elements_ont1;

            //B-A Trace
            $sim_b_a = $similar_elements / $amount_of_elements_ont2;

            $similarity_score = max($sim_a_b, $sim_b_a);

            return $similarity_score;
        }

        else
        {
            return 0;
        }
    }
}
