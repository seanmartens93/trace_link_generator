@extends('template')

@section('content')
    <div class="col-lg-12">
    <h1>
        User Stories
    </h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Description</td>
            </tr>
        </thead>
        <tbody>
            @foreach($userStories as $user_story)
                <tr>
                    <td>{{ $user_story->id }}</td>
                    <td>{{ $user_story->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h1>
        Classes
    </h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>View</td>
            </tr>
        </thead>
        <tbody>
            @foreach($classes as $class)
                <tr>
                    <td>{{ $class->id }}</td>
                    <td>{{ $class->name }}</td>
                    <td><a href="{{url('classes/' . $class->id)}}" class="btn btn-default btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h1>
        Methods
    </h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Class</td>
                <td>View</td>
            </tr>
        </thead>
        <tbody>
            @foreach($methods as $method)
                <tr>
                    <td>{{ $method->id }}</td>
                    <td>{{ $method->name }}</td>
                    <td>{{ $method->codeClass->name }}</td>
                    <td><a href="{{url('methods/' . $method->id)}}" class="btn btn-default btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection