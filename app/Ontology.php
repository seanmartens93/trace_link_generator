<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ontology extends Model
{
    //
    public function owl_classes()
    {
        return $this->hasMany('App\OWL_Class');
    }
}
