<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owl_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->integer('ontology_id')->unsigned()->index()->nullable();
            $table->foreign('ontology_id')->references('id')->on('ontologies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owl_classes');
    }
}
