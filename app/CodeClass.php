<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeClass extends Model
{
    public $primarykey = 'id';
    
    public function codeClassType()
    {
        return $this->hasOne('App\CodeClass', 'class_type_id');
    }

    public function domainConcepts()
    {
        return $this->hasMany('App\DomainConcept');
    }

    public function methods()
    {
        return $this->hasMany('App\Method');
    }
}
