<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OWL_Class extends Model
{
    //    
    public function ontology()
    {
        return $this->belongsTo('App\Ontology');
    }

    protected $table = 'owl_classes';
}
