<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraceLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trace_links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_type');
            $table->string('target_type');
                        
            $table->integer('origin_code_class_id')->unsigned()->index()->nullable();
            $table->foreign('origin_code_class_id')->references('id')->on('code_classes');

            $table->integer('origin_method_id')->unsigned()->index()->nullable();
            $table->foreign('origin_method_id')->references('id')->on('methods');

            $table->integer('origin_user_story_id')->unsigned()->index()->nullable();
            $table->foreign('origin_user_story_id')->references('id')->on('user_stories');
                        
            $table->integer('target_code_class_id')->unsigned()->index()->nullable();
            $table->foreign('target_code_class_id')->references('id')->on('code_classes');

            $table->integer('target_method_id')->unsigned()->index()->nullable();
            $table->foreign('target_method_id')->references('id')->on('methods');

            $table->integer('target_user_story_id')->unsigned()->index()->nullable();
            $table->foreign('target_user_story_id')->references('id')->on('user_stories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trace_links');
    }
}
