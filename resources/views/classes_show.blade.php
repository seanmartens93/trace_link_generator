@extends('template')

@section('content')

    <h1>Class #{{ $class->id }}: {{ $class->name }}</h2>

    <h1>Methods</h2>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($class->methods as $method)
                <tr>
                    <td>{{ $method->id }}</td>
                    <td>{{ $method->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h1>Domain Concepts</h2>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($class->domainConcepts as $concept)
                <tr>
                    <td>{{ $concept->id }}</td>
                    <td>{{ $concept->concept_name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection