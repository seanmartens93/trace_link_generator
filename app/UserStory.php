<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStory extends Model
{
    public $primarykey = 'id';

    public function domainConcepts()
    {
        return $this->hasMany('App\DomainConcept');
    }    
}
