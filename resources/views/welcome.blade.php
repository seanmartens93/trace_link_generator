@extends('template')

@section('content')

    <div class="title m-b-md" style="padding-top: 40%">
        Trace Link Generator
    </div>

    <div class="links">
        <a href="https://cs.uu.nl">Utrecht University</a>
        <a href="https://interactivenarrator.science.uu.nl/">Grimm Framework</a>
    </div>

@endsection