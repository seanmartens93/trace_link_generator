<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\CodeClass;
use App\Method;
use App\DomainConcept;

use Illuminate\Support\Facades\DB;

class ParseClasses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parseclasses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses Classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parseClasses();
    }

    public function parseClasses()
    {
        $this->parseClass(Storage::get('EventModel.php'), "EventModel", 1);
        $this->parseClass(Storage::get('GroupModel.php'), "GroupModel", 1);
        $this->parseClass(Storage::get('UserModel.php'), "UserModel", 1);

        $this->parseClass(Storage::get('EventController.php'), "EventController", 3);
        $this->parseClass(Storage::get('GroupController.php'), "GroupController", 3);
        $this->parseClass(Storage::get('ReportController.php'), "ReportController", 3);
        $this->parseClass(Storage::get('UserController.php'), "UserController", 3);

        $this->parseClass(Storage::get('CreateEventTest.php'), "CreateEventTest", 4);
        $this->parseClass(Storage::get('CreateGroupTest.php'), "CreateGroupTest", 4);
        $this->parseClass(Storage::get('CreateUserApiTest.php'), "CreateUserApiTest", 4);
        $this->parseClass(Storage::get('CreateUserTest.php'), "CreateUserTest", 4);
        $this->parseClass(Storage::get('DeleteEventTest.php'), "DeleteEventTest", 4);
        $this->parseClass(Storage::get('DeleteGroupTest.php'), "DeleteGroupTest", 4);
        $this->parseClass(Storage::get('DeleteUserTest.php'), "DeleteUserTest", 4);
        $this->parseClass(Storage::get('UpdateEventTest.php'), "UpdateEventTest", 4);
        $this->parseClass(Storage::get('UpdateGroupTest.php'), "UpdateGroupTest", 4);
        $this->parseClass(Storage::get('UpdateUserTest.php'), "UpdateUserTest", 4);

    }

    public function parseClass($class, $class_name, $class_type)
    {
        $file_array = explode("\n", $class);

        $class_id = DB::table('code_classes')->insertGetId([
            'name' => $class_name,
            'class_type_id' => $class_type
        ]);

        $current_class = CodeClass::find($class_id);

        foreach($file_array as $line) {
            $linefix = substr($line, 0, -1);

            $words = array_values(array_filter(explode(" ", $linefix)));

            if (count($words) != 0)
            {
                if ($words[0] == "class")
                {
                    $domain_concepts = $this->extractClassDomainConcepts($words);

                    foreach($domain_concepts as $concept)
                    {
                        $concept_id = DB::table('domain_concepts')->insertGetId([
                            'concept_name' => $concept,
                            'code_class_id' => $class_id
                        ]);
                    }
                }

                if ($words[0] == "public")
                {
                    if ($words[1] == "function")
                    {
                        $domain_concepts = $this->extractMethodDomainConcepts($words);

                        $method_name = implode($domain_concepts);

                        $method_id = DB::table('methods')->insertGetId([
                            'name' => $method_name,
                            'code_class_id' => $class_id
                        ]);

                        $current_method = Method::find($method_id);

                        foreach($domain_concepts as $concept)
                        {
                            $concept_id = DB::table('domain_concepts')->insertGetId([
                                'concept_name' => $concept,
                                'method_id' => $method_id
                            ]);
                        }
                    }
                }
            }
        }
    }

    public function extractClassDomainConcepts($words)
    {
        $domain_concepts = [];

        foreach($words as $w)
        {
            $fullword = preg_replace('/[^\p{L}\p{N}\s]/u', '', $w);

            $split_words = array_values(array_filter(preg_split('/(?=[A-Z])/', $fullword)));

            foreach($split_words as $word)
            {
                if ($word != "class" && $word != "extends" && $word != "Model" && $word != "Controller" && $word != "Test" && $word != "Case")
                {
                    array_push($domain_concepts, $word);
                }
            }
        }

        return $domain_concepts;
    }

    public function extractMethodDomainConcepts($words)
    {
        $domain_concepts = [];

        foreach($words as $w)
        {
            $word = preg_replace('/[^\p{L}\p{N}\s]/u', '', $w);

            $split_words = preg_split('/(?=[A-Z])/', $word);

            foreach($split_words as $sw)

            if ($sw != "public" && $sw != "function" && $sw != "private" && $sw != "protected")
            {
                array_push($domain_concepts, $sw);
            }
        }

        return $domain_concepts;
    }
}
